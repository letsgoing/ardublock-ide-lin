# letsgoING ArduBlock Arduino IDE - Linux
Um direkt mit dem Programmieren loslegen zu können, haben wir Dir für alle gängigen Betriebssysteme eine [Arduino IDE](https://www.arduino.cc/) vorbereitet.
- [Windows](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-win)
- [Mac](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-mac)
- [Linux](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-lin)

## Download
Über den Button auf der rechten Seite kannst Du den Download starten. Wähle einfach ein passendes Dateiformat und der Download startet von selbst.

![](DownloadButton.png) 

Nach dem Download die Datei entpacken und in den Ordner wechseln.
Anschließend den Punkt Setup ausführen.

## Setup
Damit das einrichten möglichst einfach kannst Du wie folgt vorgehen:
1. Den Ordner Arduino_Lin auf das lokale Arbeitsverzeichnis ziehen.
2. Terminal in diesem Verzeichnis Öffnen.
3. Den folgenden Befehl "sh setup.sh" ausführen.
4. In dem Arduino Unterverzeichnis ist das ausführbare Arduino-Programm mit Ardublock.

## Entwicklung
Damit unsere Entwicklungen schnellstmöglich zum Einsatz kommen können findet ihr die aktuelle Version [hier](https://gitlab.reutlingen-university.de/letsgoing/ardublock-ide-lin/-/tree/dev/).
Aktuell haben wir folgende Punkte eingearbeitet:

- einen Simulator für alle Grundlagenmodule
- in der Größe verstellbares Menü
- allgemeine Verbesserungen und Fehlerbehebungen 


Viel Spaß,

Euer letsgoING-Team
